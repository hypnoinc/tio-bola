
module.controller('estoqueController', function($scope) {

    $scope.pedidos = [];

    /* Função que inicializa a tela com a lista de tipos de produtos */
    $scope.initTiposList = function() {
        $scope.produtoTipo = [];
        $scope.ajax('getProdutoTipoList',{idBar: $scope.idBar}, 'produtoTipoList');
    };

    $scope.produtoTipoList = function(retorno) {
        if (retorno.status == 'ok'){
            $scope.produtoTipo = retorno.list;
            $scope.$digest();
    	} else
            $scope.displayError(retorno);
    };


    /* Função que inicializa a tela com a lista de produtos */
    $scope.initProdutosList = function(idTipo) {
        $scope.produtos = [];
        if(myNavigator.topPage.data && myNavigator.topPage.data.tipo)
            $scope.nmTipo = myNavigator.topPage.data.tipo;
        $scope.ajax('getProdutoList',{idBar: $scope.idBar, idTipo: idTipo}, 'produtoList');
    };

    $scope.produtoList = function(retorno) {
        if (retorno.status == 'ok'){
            $scope.produtos = retorno.list;
            $scope.$digest();
    	} else
            $scope.displayError(retorno);
    };

    /* Função que inicializa a tela com a lista de produtos */
    $scope.getProduto = function(idProduto) {
        $scope.produto = {};
        $scope.ajax('getProduto',{idProduto: idProduto}, 'produtoLoad');
    };

    $scope.produtoLoad = function(retorno) {
        if (retorno.status == 'ok'){
            $scope.produto = retorno.produto;
            $scope.$digest();
    	} else
            $scope.displayError(retorno);
    };

    $scope.adicionarCarrinho = function(produto) {
        if (produto.estoque>0) {
            carrinhoDiv = document.getElementById('carrinho');
            carrinhoDiv.style.display = 'block';
            $scope.carrinho.valor = parseFloat($scope.carrinho.valor)+parseFloat(produto.valor);
            $scope.carrinho.percentual = (parseFloat($scope.carrinho.valor)*100)/parseFloat($scope.freteGratis);
            if($scope.carrinho.percentual>=100){
                carrinhoDiv = document.getElementById('labelFreteGratis');
                carrinhoDiv.style.color = '#10F110';
                carrinhoDiv.style.fontSize = '16px';
                carrinhoDiv.innerHTML = 'Frete Gr&aacute;tis'
                $scope.carrinho.frete = 0;
            }
            var add = false;
            for(i=0;i < $scope.carrinho.produtos.length; i++){
                if ($scope.carrinho.produtos[i].id_produto == produto.id_produto){
                    $scope.carrinho.produtos[i].qtPedido++;
                    $scope.carrinho.produtos[i].vlPedido = parseFloat($scope.carrinho.produtos[i].vlPedido)+parseFloat(produto.valor);
                    add = true;
                }
            }
            if(!add){
                produto.qtPedido = 1;
                produto.vlPedido = produto.valor;
                $scope.carrinho.produtos.push(produto);
            }
        } else {
            ons.notification.alert('Produto sem estoque.');
        }
    };

    /* Função que inicializa a tela com a lista de produtos */
    $scope.getPedidos = function(idTipo) {
        $scope.pedidos = [];
        if ($scope.threadPedidos) {
            $scope.ajax('getPedidos',{idBar: $scope.idBar, idClinente: $scope.usuario.id_usuario}, 'pedidoList');
            setInterval(function(){ $scope.ajax('getPedidos',{idBar: $scope.idBar, idClinente: $scope.usuario.id_usuario}, 'pedidoList', true); }, 3000);
            $scope.threadPedidos = false;
        }
    };

    $scope.pedidoList = function(retorno) {
        if (retorno.status == 'ok'){
            $scope.pedidos = retorno.list;
            $scope.$digest();
    	} else
            $scope.displayError(retorno);
    };

    $scope.solicitarProduto = async function(produto) {
        var retorno = await ons.notification.confirm(
            'Tem certeza que deseja pedir o produto?<br><br>(Tem que estar no bar para soliciar)',
            { buttonLabels: ['<span class="font-vermelha">Cancelar</span>',
                             '<span class="font-verde">Pedir</span>'] }
        );
        if(retorno){
            $scope.ajax('solicitarProduto', {id: produto}, 'solicitarReturn');
        }
    }
});
