
module.controller('usuarioController', function($scope) {

    $scope.usuarioGet = function(idTipo) {
        $scope.ajax('getUsuario', $scope.usuario, 'usuarioLoad');
    };

    $scope.usuarioLoad = function(retorno) {
        if (retorno.status == 'ok'){
            $scope.data = retorno.usuario;
            $scope.$digest();
    	} else
            $scope.displayError(retorno);
    };

});
