
module.controller('eventoController', function($scope) {

    $scope.pedidos = [];

    /* Função que inicializa a tela com a lista de tipos de produtos */
    $scope.initEventos = function() {
        $scope.eventos = [];
        $scope.ajax('getEventos',{idBar: $scope.idBar}, 'eventosList');
    };

    $scope.eventosList = function(retorno) {
        if (retorno.status == 'ok'){
            $scope.eventos = retorno.list;
            $scope.$digest();
    	} else
            $scope.displayError(retorno);
    };

});
