ons.platform.select('android');
var module = ons.bootstrap();
module.controller('barController', function($scope) {

    $scope.appNome = 'Tio Bola';
    $scope.data    = null;
    $scope.idBar   = 1;

    $scope.carrinho = {
        produtos: [],
        valor: 0,
        percentual: 0,
        frete: 10
    }
    $scope.retirarBar = false;
    $scope.freteGratis = 50;
    $scope.freteValor  = 10;
    $scope.threadPedidos = true;

    $scope.init = function() {
        $scope.nome = 'App Bar';
        $scope.divLoad = document.getElementById('divLoad');
        $scope.usuario = Disco.get('usuario');
        //myNavigator._events.postpush.pop();
    };

    $scope.initHome = function() {
        $scope.homeCarrosel = [];
        $scope.homeBanners = [];
        $scope.ajax('getHome',{idBar: $scope.idBar}, 'homeInit');
    };

    $scope.homeInit = function(retorno) {
        if (retorno.status == 'ok'){
            for(var i=0; i < retorno.list.length; i++) {
                if (i < 3)
                    $scope.homeCarrosel[$scope.homeCarrosel.length] = retorno.list[i];
                else
                    $scope.homeBanners[$scope.homeBanners.length] = retorno.list[i];
            }
            $scope.$digest();
    	} else
            $scope.displayError(retorno);
    };

    $scope.ajax = function(target, parameter, funcCallBack, skipLoad) {

        if(!skipLoad)
            $scope.divLoad.style.display = 'inline';

		var urlServer = "https://hypnostartup.com/api/bar/hub.php";

        if(!parameter)
			parameter = 0;

        var request = new XMLHttpRequest();
        request.open('POST', urlServer);
        request.onreadystatechange = () => {
        	if (request.readyState == 4){
                $scope.divLoad.style.display = 'none';
				if(request.status == 200){
					if(typeof this[funcCallBack] == 'function')
                        if ($scope.IsJsonString(request.responseText))
	            		    this[funcCallBack](JSON.parse(request.responseText));
                        else
                            this[funcCallBack]({err: 'responseText Fail'});
				}else{
                    console.log(1, 'Falha na Requisição');
                }
        	}
		}
        request.onerror = err => {
            console.log(2, 'Err A02 ', err);
            $scope.divLoad.style.display = 'none';
        }

        request.send(JSON.stringify({target: target, parametros: parameter}));
    };

    $scope.IsJsonString = function(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }

    $scope.loginReturn = function(retorno) {
        if (retorno.status == 'ok') {
            $scope.login = true;
            $scope.user = retorno;
            Disco.set('user', retorno);
            Disco.set('logado', true);
            myNavigator.resetToPage('views/home.html');
        } else {
            $scope.displayError(retorno);
        }
    };

    $scope.logout = function() {
        Disco.set('usuario', null);
        $scope.usuario = null;
        $scope.loadMenu('views/menu-novidades.html');
    };

    $scope.loadMenu = function(page) {
        mySplitter.content.load(page).then(function() {
            mySplitter.left.close();
        });
    }

    $scope.load = function(page, data){
        myNavigator.pushPage('views/'+page, {data: data});
    };

    $scope.replace = async function(page) {
        await myNavigator.removePage(myNavigator.pages.length-1,{animation: 'none'});
        myNavigator.pushPage('views/'+page,{animation: 'none'});
    };

    $scope.dataClear = function(){
        $scope.data   = null;
        $scope.search = '';
    }

    $scope.displayError = function(retorno){
        if(retorno.msg)
            ons.notification.alert(retorno.msg);
        else
            ons.notification.alert('Erro ao se comunicar com o servidor');
    }

    $scope.formatDtHrSQL2BR = function(data, formato) {
        var dataHora  = data.split(" ")
        var dataSplit = dataHora[0].split("-");
        var horaSplit = dataHora[1].split(":");
        var dia  = dataSplit[2];
        var mes  = dataSplit[1];
        var ano  = dataSplit[0];
        var hora = horaSplit[0];
        var min  = horaSplit[1];

        switch (formato) {
            case 'd':
                return dia+'/'+mes+'/'+ano;
                break;
            default:
                return dia+'/'+mes+'/'+ano+' '+hora+':'+min
        }
    };

    $scope.formatDtSQL2BR = function(data) {
        var dataSplit  = data.split("-")
        var dia  = dataSplit[2];
        var mes  = dataSplit[1];
        var ano  = dataSplit[0];
        return dia+'/'+mes+'/'+ano;
    };

    $scope.abrirDetalhes = function() {
        carrinhoDiv = document.getElementById('carrinho');
        carrinhoDiv.style.height = '400px';
        detalhesCarrinho = document.getElementById('detalhesCarrinho');
        detalhesCarrinho.style.display = 'block';
        btnDetalhes = document.getElementById('btnDetalhes');
        btnDetalhes.style.display = 'none';
        btnDetalhesFechar = document.getElementById('btnDetalhesFechar');
        btnDetalhesFechar.style.display = 'block';
    };

    $scope.fecharDetalhes = function() {
        detalhesCarrinho = document.getElementById('detalhesCarrinho');
        detalhesCarrinho.style.display = 'none';
        carrinhoDiv = document.getElementById('carrinho');
        carrinhoDiv.style.height = '75px';
        btnDetalhes = document.getElementById('btnDetalhes');
        btnDetalhes.style.display = 'block';
        btnDetalhesFechar = document.getElementById('btnDetalhesFechar');
        btnDetalhesFechar.style.display = 'none';
    };

    $scope.removerCarrinho = function(idProduto) {
        for(i=0; i < $scope.carrinho.produtos.length; i++){
            if ($scope.carrinho.produtos[i].id_produto == idProduto){
                $scope.carrinho.produtos[i].qtPedido--;
                $scope.carrinho.valor = parseFloat($scope.carrinho.valor)-parseFloat($scope.carrinho.produtos[i].valor);
                $scope.carrinho.percentual = (parseFloat($scope.carrinho.valor)*100)/parseFloat($scope.freteGratis);
                if(!$scope.retirarBar && $scope.carrinho.percentual<100){
                    carrinhoDiv = document.getElementById('labelFreteGratis');
                    carrinhoDiv.style.color = '#e9e9e9';
                    carrinhoDiv.style.fontSize = '14px';
                    carrinhoDiv.innerHTML = 'Frete R$ '+$scope.freteValor
                    $scope.carrinho.frete = $scope.freteValor;
                }
                if($scope.carrinho.produtos[i].qtPedido == 0){
                    $scope.carrinho.produtos.splice(i,1);
                } else{
                    $scope.carrinho.produtos[i].vlPedido = parseFloat($scope.carrinho.produtos[i].vlPedido)-parseFloat($scope.carrinho.produtos[i].valor);
                }
                break;
            }
        }
        if($scope.carrinho.produtos.length==0){
            $scope.fecharDetalhes();
            carrinhoDiv = document.getElementById('carrinho');
            carrinhoDiv.style.display = 'none';
        }
    };
    $scope.adicionarCarrinho2 = function(produto) {
        carrinhoDiv = document.getElementById('carrinho');
        carrinhoDiv.style.display = 'block';
        $scope.carrinho.valor = parseFloat($scope.carrinho.valor)+parseFloat(produto.valor);
        $scope.carrinho.percentual = (parseFloat($scope.carrinho.valor)*100)/parseFloat($scope.freteGratis);
        if(!$scope.retirarBar && $scope.carrinho.percentual>=100){
            carrinhoDiv = document.getElementById('labelFreteGratis');
            carrinhoDiv.style.color = '#10F110';
            carrinhoDiv.style.fontSize = '16px';
            carrinhoDiv.innerHTML = 'Frete Gr&aacute;tis'
            $scope.carrinho.frete = 0;
        }
        var add = false;
        for(i=0;i < $scope.carrinho.produtos.length; i++){
            if ($scope.carrinho.produtos[i].id_produto == produto.id_produto){
                $scope.carrinho.produtos[i].qtPedido++;
                $scope.carrinho.produtos[i].vlPedido = parseFloat($scope.carrinho.produtos[i].vlPedido)+parseFloat(produto.valor);
                add = true;
            }
        }
        if(!add){
            produto.qtPedido = 1;
            produto.vlPedido = produto.valor;
            $scope.carrinho.produtos.push(produto);
        }
    };

    $scope.retirarSwitch = function(status){
        $scope.retirarBar = status;
        if(status){
            labelFreteGratis = document.getElementById('labelFreteGratis');
            labelFreteGratis.innerHTML = '';
            $scope.carrinho.frete = 0;
        } else {
            labelFreteGratis = document.getElementById('labelFreteGratis');
            if($scope.carrinho.percentual<100){
                labelFreteGratis.style.color = '#e9e9e9';
                labelFreteGratis.style.fontSize = '14px';
                labelFreteGratis.innerHTML = 'Frete R$ '+$scope.freteValor
                $scope.carrinho.frete = $scope.freteValor;
            } else{
                labelFreteGratis.style.color = '#10F110';
                labelFreteGratis.style.fontSize = '16px';
                labelFreteGratis.innerHTML = 'Frete Gr&aacute;tis'
                $scope.carrinho.frete = 0;
            }
        }

    }

    $scope.finalizarCarrinho = function() {
                if ($scope.usuario) {
                    finalizaPedido.show();
                } else {
                    $scope.fecharDetalhes();
                    $scope.enviarPedido = true;
                    $scope.data = null;
                    $scope.load('cadastro.html');
                }
    }

    $scope.enviarCarrinho = function(endData) {
        $scope.ajax('realizarPedido',
                     {idBar: $scope.idBar,
                      idUsuario: $scope.usuario.id_usuario,
                      nome: $scope.usuario.nome,
                      valor: $scope.carrinho.valor,
                      produtos: $scope.carrinho.produtos,
                      retirarBar: $scope.retirarBar,
                      formaPagamento: endData.formaPagamento,
                      observacao: endData.observacao,
                      troco: endData.troco},
                      'retornoPedido');
    }

    $scope.cadastroEnviar = function(data){
        $scope.ajax('addUser',
                     {idBar: $scope.idBar,
                      id_usuario: data.id_usuario,
                      nome: data.nome,
                      nome_entrega: data.nome_entrega,
                      email: data.email,
                      senha: data.senha,
                      telefone: data.telefone,
                      endereco: data.endereco,
                      numero: data.numero,
                      bairro: data.bairro,
                      cep: data.cep,
                      referencia: data.referencia},
                      'retronoCadastro');
    }

    $scope.cadastroEntrar = function(data){
        $scope.ajax('login',
                     {idBar: $scope.idBar,
                      email: data.email,
                      senha: data.senha},
                      'retronoCadastro');
    }

    $scope.retronoCadastro = function(retorno) {
        if (retorno.status == 'ok'){
            Disco.set('usuario', retorno);
            $scope.usuario = retorno;
            $scope.loadMenu('views/menu-novidades.html');
            if ($scope.enviarPedido && $scope.carrinho.produtos.length > 0)
                finalizaPedido.show();
            $scope.$digest();
    	} else
            $scope.displayError(retorno);
    };
    $scope.retornoPedido = function(retorno) {
        if (retorno.status == 'ok'){
            //Limpa o carrinho
            $scope.carrinho = {
                produtos: [],
                valor: 0,
                percentual: 0,
                frete: 10
            }
            finalizaPedido.hide();
            $scope.carrinho.frete = $scope.freteValor;
            $scope.fecharDetalhes();
            carrinhoDiv = document.getElementById('carrinho');
            carrinhoDiv.style.display = 'none';

            $scope.loadMenu('views/pedidos.html');
            $scope.$digest();
    	} else
            $scope.displayError(retorno);
    };

});
